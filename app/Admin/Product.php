<?php

Admin::model('App\Product')->title('My Prducts')->display(function ()
{
	$display = AdminDisplay::datatables();
	$display->with('category');
	$display->filters([

	]);
	$display->columns([
		Column::string('name')->label('Name'),
		Column::string('category.name')->label('Category'),
		Column::string('weight')->label('Weight'),
		Column::string('price')->label('Price'),
		Column::image('image')->label('Image'),
		Column::string('views')->label('Views'),
		Column::string('sell')->label('Sell'),
		Column::custom()->label('Special')->callback(function ($instance)
		{
			return $instance->special == 1 ? 'Yes' : 'No';
		}),
		Column::custom()->label('Status')->callback(function ($instance)
		{
			return $instance->status == 1 ? 'Active' : 'Deactive';
		}),
		Column::custom()->label('Best')->callback(function ($instance)
		{
			return $instance->best == 1 ? 'Yes' : 'No';
		}),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('name', 'Name'),
		FormItem::select('category_id', 'Category')->model('App\Category')->display('name'),
		FormItem::text('weight', 'Weight'),
		FormItem::text('price', 'Price'),
		FormItem::image('image', 'Image'),
		FormItem::text('views', 'Views'),
		FormItem::text('sell', 'Sell'),
		FormItem::select('status', 'Status')->options([1 => 'Active', 0 => 'Deactive']),
		FormItem::checkbox('special', 'Special'),
		FormItem::checkbox('best', 'Best'),
		FormItem::ckeditor('description', 'Description'),
	]);
	return $form;
});