<?php

Admin::model('App\Banner')->title('My Banners(169x308)')->display(function ()
{
	$display = AdminDisplay::datatables();
	$display->with();
	$display->filters([

	]);
	$display->columns([
		Column::image('image')->label('Image'),
		Column::custom()->label('Status')->callback(function ($instance)
		{
			return $instance->status == 1 ? 'Active' : 'Deactive';
		}),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::image('image', 'Image'),
		FormItem::select('status', 'Status')->options([1 => 'Active', 0 => 'Deactive'])
	]);
	return $form;
});