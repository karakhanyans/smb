<?php

Admin::model('App\Category')->title('Categories')->display(function ()
{
	$display = AdminDisplay::datatables();
	$display->with();
	$display->filters([

	]);
	$display->columns([
		Column::string('id')->label('Id'),
		Column::string('name')->label('Name'),
		Column::string('slug')->label('Slug'),
		Column::image('image')->label('Image'),
		Column::custom()->label('Status')->callback(function ($instance)
		{
			return $instance->status == 1 ? 'Active' : 'Deactive';
		}),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('name', 'Name'),
		FormItem::text('slug', 'Slug'),
		FormItem::image('image', 'Image'),
		FormItem::select('status', 'Status')->options([1 => 'Active', 0 => 'Deactive'])
	]);
	return $form;
});