<?php

function color(){
    $rand = dechex(rand(0x000000, 0xFFFFFF));
    return '#' . $rand;
}
function thumb($image){
    $thumb = explode('/',$image);
    $newPath = 'images/uploads/thumbs/'.end($thumb);
    return $newPath;
}
function special($products){
    $special = [];
    foreach ($products as $product) {
        if($product->special == 1){
            array_push($special,$product);
        }
    }
    shuffle($special);
    $special = array_slice($special,0,5);
    return $special;
}
function news($products){
    return $products->slice(0,4)->shuffle();
}
function most_viewed($products){
    return $products->slice(0,4)->shuffle();
}
function best($products){
    $best = [];
    foreach ($products as $product) {
        if($product->best == 1){
            $best[$product->category->name][] = $product;
        }
    }
    return $best;

}

