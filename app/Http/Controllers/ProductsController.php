<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$categories = Category::active()->has('products')->get();
		$products = Product::active()->with('category')->orderBy('created_at','DESC')->get();
		$specials = special($products);
		$product = Product::with('category')->active()->find($id);
		if(!Session::has('products')){
			Session::push('products',$id);
		}else{
			$session = session('products');
			if(!in_array($id,$session)){
				Session::push('products',$id);
				$product->views += 1;
			}
		}
		$product->save();
		$sames = Product::same($product->category_id)
			->active()
			->orderBy('views','DESC')
			->get();
		return view('products.product',compact('categories','product','sames','specials'));
	}
	public function all(){
		$categories = 	Category::active()->has('products')->get();
		$products = Product::active()->paginate(9);
		$recent_views = '';

		if(Session::has('products')){
			$recent_views_array = array_slice(session('products'),0,5);
			$recent_views = Product::active()->with('category')->whereIn('id',$recent_views_array)->get();
		}

		return view('categories.index',compact('categories','products','recent_views'))->with([
			'category_name' => ''
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
