<?php namespace App\Http\Controllers;
use App\Category;
use App\Product;
use App\Slider;
use App\Banner;
use Illuminate\Support\Facades\Session;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = 	Category::active()->has('products')->get();
		$products = 	Product::active()->with('category')->orderBy('created_at','DESC')->get();
		$most_viewed = 	Product::active()->with('category')->orderBy('views','DESC')->limit(4)->get();
		$sliders = 		Slider::active()->get();
		$banners = 		Banner::active()->get();

		$specials = special($products);
		$news = news($products);
		$products = $products->slice(0,16);
		$bests = best($products);
		$recent_views = '';

		if(Session::has('products')){
			$recent_views_array = array_slice(session('products'),0,3);
			$recent_views = Product::active()->with('category')->whereIn('id',$recent_views_array)->get();
		}


		return view('home.index',compact(
			'categories',
			'products',
			'news',
			'sliders',
			'banners',
			'specials',
			'most_viewed',
			'bests',
			'recent_views'
		));
	}

}
