<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    public function scopeActive($query){
        return $query->where('status',1);
    }
    public function scopeSame($query,$category_id){
        return $query->where('category_id',$category_id);
    }
    public function scopeSpecial($query){
        return $query->where('special',1);
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
	}

    public function scopeByCategory($query, $slug)
    {
       return $query->whereHas('category', function($q) use ($slug){
        $q->where('slug',$slug);
       });
    }

}
