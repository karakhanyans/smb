@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-lg-3">
                    <!-- Block vertical-menu -->
                    <div class="block block-vertical-menu">
                        <div class="vertical-head">
                            <h5 class="vertical-title">Բաժիններ</h5>
                        </div>
                        <div class="vertical-menu-content">
                            <ul class="vertical-menu-list">
                                @foreach($categories as $category)
                                    <li class="{{ (strstr(Request::path(), 'category/'.$category->slug)) ? 'active' : '' }}" style="border-color:{{ color() }}"><a href="{{ url('category',$category->slug) }}">{{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- ./Block vertical-menu -->
                    @if(!empty($recent_views))
                        <div class="block block-specail3">
                            <div class="block-head">
                                <h4 class="widget-title">Վերջին դիտվածները</h4>
                            </div>
                            <div class="block-inner">
                                <ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"768":{"items":1}}'>
                                    @foreach($recent_views as $recent)
                                        <li class="product">
                                            <div class="product-container">
                                                <div class="product-left">
                                                    <div class="product-thumb">
                                                        <a class="product-img" href="{{ url('product',$recent->id) }}"><img src="{{ asset(thumb($recent->image)) }}" alt="{{ $recent->name }}"></a>
                                                        <a title="Quick View" href="{{ url('product',$recent->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
                                                    </div>
                                                    @if($recent->sell > 0)
                                                        <div class="product-status">
                                                            <span class="new">զեղչ</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="product-right">
                                                    <div class="product-name">
                                                        <a href="{{ url('product',$recent->id) }}">{{  str_limit($recent->name, $limit = 30, $end = '...') }}</a>
                                                    </div>
                                                    <div class="price-box">
                                                        @if($recent->sell > 0)
                                                            <span class="product-price">{{ $recent->sell }} դր</span>
                                                            <span class="product-price-old">{{ $recent->price }} դր</span>
                                                        @else
                                                            <span class="product-price">{{ $recent->price }} դր</span>
                                                        @endif
                                                    </div>
                                                    <div class="product-star">
                                                        <span>{{ $recent->category->name }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <h3 class="page-title">
                        <span>{{ $category_name }}</span>
                    </h3>
                    <div class="sortPagiBar">
                        <div class="sortPagiBar-inner">
                            <nav>
                                <ul class="pagination">
                                    <?php echo $products->render(); ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="category-products">
                        <ul class="products row">
                            @foreach($products as $product)
                                <li class="col-xs-12 col-sm-6 col-md-4 product">
                                    <div class="product-container categories">
                                        <div class="product-left">
                                            <div class="product-thumb">
                                                <a class="product-img" href="{{ url('product',$product->id) }}"><img src="{{ asset(thumb($product->image)) }}" alt="{{ $product->name }}"></a>
                                                <a title="Դիտել" href="{{ url('product',$product->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
                                            </div>
                                            @if($product->sell > 0)
                                                <div class="product-status">
                                                    <span class="new">զեղչ</span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="product-right">
                                            <div class="product-name">
                                                <a href="{{ url('product',$product->id) }}">{{ str_limit($product->name, $limit = 45, $end = '...') }}</a>
                                            </div>

                                            <div class="price-box">
                                                @if($product->sell > 0)
                                                    <span class="product-price">{{ $product->sell }} դր</span>
                                                    <span class="product-price-old">{{ $product->price }} դր</span>
                                                @else
                                                    <span class="product-price">{{ $product->price }} դր</span>
                                                @endif
                                            </div>
                                            <div class="product-star">
                                                <span>{{ $product->category->name }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="sortPagiBar">
                        <div class="sortPagiBar-inner">
                            <nav>
                                <ul class="pagination">
                                    <?php echo $products->render(); ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection