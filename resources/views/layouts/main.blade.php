<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/reset.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('js/libs/bootstrap/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('js/libs/easyzoom/easyzoom.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('js/libs/font-awesome/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('js/libs/owl.carousel/owl.carousel.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('js/libs/jquery-ui/jquery-ui.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/global.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('css/option3.css') }}" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<title>Silver Collection</title>
</head>
<body class="option3">
<!-- header -->
<header id="header">
	<div class="container">
		<!-- main header -->
		<div class="row">
			<div class="main-header">
				<div class="row">
					<div class="col-sm-12 col-md-2 col-lg-2">
						<div class="logo">
							<a href="{{ url() }}"><img src="{{ asset('images/project/logo.jpg') }}" alt="Logo"></a>
						</div>
					</div>
					<div class="col-sm-12 col-md-6 col-lg-7">
						<div class="advanced-search box-radius">
							<form class="form-inline">
								<div class="form-group search-input">
									<input type="text" placeholder="Որոնում">
								</div>
								<button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
							</form>
						</div>
					</div>
					<div class="col-sm-7 col-md-4 col-lg-3">
						<div class="wrap-block-cl">
							<div class="inner-cl box-radius">
								<div class="dropdown user-info">
									<a data-toggle="dropdown" role="button"><i class="fa fa-2x fa-mobile-phone"></i></a>
								</div>
								<div class="language">
									<a role="button" href="tel:+37493773701">+37493 77 37 01</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./main header -->
	</div>
	<div class="container">
		<div class="row">
			<!-- main menu-->
			<div class="main-menu">
				<div class="container">
					<div class="row">
						<nav class="navbar" id="main-menu">
							<div class="container-fluid">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
										<i class="fa fa-bars"></i>
									</button>
									<a class="navbar-brand" href="#">MENU</a>
								</div>
								<div id="navbar" class="navbar-collapse collapse">
									<ul class="nav navbar-nav">
										<li class="active"><a href="{{ url() }}">Գլխավոր</a></li>
										<li><a href="{{ url() }}">Մեր մասին</a></li>
										<li><a href="{{ url() }}">Ինչպես պատվիրել</a></li>
										<li><a href="{{ url() }}">Առաքում</a></li>
										<li><a href="{{ url() }}">Կոնտակտային տվյալներ</a></li>
									</ul>
								</div><!--/.nav-collapse -->
							</div>
						</nav>
					</div>
				</div>
			</div>
			<!-- ./main menu-->
		</div>
	</div>
</header>
<!-- ./header -->
 @yield('content')
<!-- footer -->
<footer id="footer">
	<div class="footer-social">
		<div class="container">
			<div class="row">
				<div class="block-social">
					<ul class="list-social">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
						<li><a href="#"><i class="fa fa-pied-piper"></i></a></li>
						<li><a href="#"><i class="fa fa-skype"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="block-coppyright">
					© 2015 Silver Collection.
				</div>
				<div class="block-shop-phone">
					<strong>+37493 77 37 01</strong>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- ./footer -->
<a href="#" class="scroll_top" title="Դեպի վեր" style="display: inline;">Դեպի վեր</a>
<script type="text/javascript" src="{{ asset('js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/owl.carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- COUNTDOWN -->
<script type="text/javascript" src="{{ asset('js/libs/countdown/jquery.plugin.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs/countdown/jquery.countdown.js') }}"></script>
<!-- ./COUNTDOWN -->
{{--EASYZOOM--}}
<script type="text/javascript" src="{{ asset('js/libs/easyzoom/easyzoom.js') }}"></script>
{{--EASYZOOM--}}
<script type="text/javascript" src="{{ asset('js/jquery.actual.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

</body>

</html>