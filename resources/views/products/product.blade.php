@extends('layouts.main')

@section('content')
    <div class="container product-page">
        <div class="row">
            <div class="row">
                <div class="col-sm-5">
                    <div class="block block-product-image">
                        <div class="product-image easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                            <a href="{{ asset($product->image) }}">
                                <img src="{{ asset($product->image) }}" alt="Product" width="450" height="450" />
                            </a>
                        </div>
                        <div class="product-list-thumb">
                            <ul class="thumbnails kt-owl-carousel" data-margin="10" data-nav="true" data-responsive='{"0":{"items":2},"600":{"items":2},"1000":{"items":3}}'>
                                <li>
                                    <a href="{{ asset($product->image) }}" data-standard="{{ asset($product->image) }}">
                                        <img src="{{ asset($product->image) }}" alt="Product" width="450" height="450" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-12 col-md-7">
                            <div class="block-product-info">
                                <h2 class="product-name">{{ $product->name }}</h2>
                                <div class="price-box">
                                    @if($product->sell > 0)
                                        <span class="product-price">{{ $product->sell }} դր</span>
                                        <span class="product-price-old">{{ $product->price }} դր</span>
                                    @else
                                        <span class="product-price">{{ $product->price }} դր</span>
                                    @endif
                                </div>
                                <div class="variations-box">
                                    <table class="variations-table">
                                        <tr>
                                            <td class="table-label">Քաշ</td>
                                            <td class="table-value">
                                                <ul class="list-check-box">
                                                    <li><a href="#">{{ $product->weight }} գր</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-label">Проба</td>
                                            <td class="table-value"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-5">
                            <div class="block block-specail3">
                                <div class="block-head">
                                    <h4 class="widget-title">Պահանջված</h4>
                                </div>
                                <div class="block-inner">
                                    <ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"768":{"items":1}}'>
                                        @foreach($specials as $special)
                                            <li class="product">
                                                <div class="product-container">
                                                    <div class="product-left">
                                                        <div class="product-thumb">
                                                            <a class="product-img" href="{{ url('product',$special->id) }}"><img src="{{ asset(thumb($special->image)) }}" alt="{{ $special->name }}"></a>
                                                            <a title="Quick View" href="{{ url('product',$special->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
                                                        </div>
                                                        @if($special->sell > 0)
                                                            <div class="product-status">
                                                                <span class="new">զեղչ</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="product-right">
                                                        <div class="product-name">
                                                            <a href="{{ url('product',$special->id) }}">{{  str_limit($special->name, $limit = 30, $end = '...') }}</a>
                                                        </div>
                                                        <div class="price-box">
                                                            @if($special->sell > 0)
                                                                <span class="product-price">{{ $special->sell }} դր</span>
                                                                <span class="product-price-old">{{ $special->price }} դր</span>
                                                            @else
                                                                <span class="product-price">{{ $special->price }} դր</span>
                                                            @endif
                                                        </div>
                                                        <div class="product-star">
                                                            <span>{{ $special->category->name }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="block block-tabs tab-left">
                                <div class="block-inner">
                                    <div class="tab-container">
                                        <div id="tab-1" class="tab-panel active">
                                            <p>
                                                {!! $product->description  !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Product tab -->

            <!-- Product tab -->
            <!-- Related Products -->
            <div class="block block-products-owl">
                <div class="block-head">
                    <div class="block-title">
                        <div class="block-title-text text-lg">Նույն բաժնից</div>
                    </div>
                </div>
                <div class="block-inner">
                    <ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":8}}'>
                        @foreach($sames as $same)
                            <li class="product">
                                <div class="product-container">
                                    <div class="product-left">
                                        <div class="product-thumb">
                                            <a class="product-img" href="{{ url('product',$same->id) }}"><img src="{{ asset(thumb($same->image)) }}" alt="{{ $same->name }}"></a>
                                            <a title="Quick View" href="{{ url('product',$same->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
                                        </div>
                                        @if($same->sell > 0)
                                            <div class="product-status">
                                                <span class="new">զեղչ</span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="product-right">
                                        <div class="product-name">
                                            <a href="{{ url('product',$same->id) }}">{{  str_limit($same->name, $limit = 30, $end = '...') }}</a>
                                        </div>
                                        <div class="price-box">
                                            @if($same->sell > 0)
                                                <span class="product-price">{{ $same->sell }} դր</span>
                                                <span class="product-price-old">{{ $same->price }} դր</span>
                                            @else
                                                <span class="product-price">{{ $same->price }} դր</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- ./Related Products -->
        </div>
    </div>
@endsection