@extends('layouts.main')

@section('content')

	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-sm-4 col-md-3 col-lg-3">
					<!-- Block vertical-menu -->
					<div class="block block-vertical-menu">
						<div class="vertical-head">
							<h5 class="vertical-title">Բաժիններ</h5>
						</div>
						<div class="vertical-menu-content">
							<ul class="vertical-menu-list">
								@foreach($categories as $category)
									<li style="border-color:{{ color() }}"><a href="{{ url('category',$category->slug) }}">{{ $category->name }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
					<!-- ./Block vertical-menu -->
					<div class="block block-specail3">
						<div class="block-head">
							<h4 class="widget-title">Պահանջված</h4>
						</div>
						<div class="block-inner">
							<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"768":{"items":1}}'>
								@foreach($specials as $special)
									<li class="product">
										<div class="product-container">
											<div class="product-left">
												<div class="product-thumb">
													<a class="product-img" href="{{ url('product',$special->id) }}"><img src="{{ thumb($special->image) }}" alt="{{ $special->name }}"></a>
													<a title="Quick View" href="{{ url('product',$special->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
												</div>
												@if($special->sell > 0)
													<div class="product-status">
														<span class="new">զեղչ</span>
													</div>
												@endif
											</div>
											<div class="product-right">
												<div class="product-name">
													<a href="{{ url('product',$special->id) }}">{{  str_limit($special->name, $limit = 30, $end = '...') }}</a>
												</div>
												<div class="price-box">
													@if($special->sell > 0)
														<span class="product-price">{{ $special->sell }} դր</span>
														<span class="product-price-old">{{ $special->price }} դր</span>
													@else
														<span class="product-price">{{ $special->price }} դր</span>
													@endif
												</div>
												<div class="product-star">
													<span>{{ $special->category->name }}</span>
												</div>
											</div>
										</div>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
					@if(!empty($recent_views))
						<div class="block block-specail3">
							<div class="block-head">
								<h4 class="widget-title">Վերջին դիտվածները</h4>
							</div>
							<div class="block-inner">
								<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":2},"768":{"items":1}}'>
									@foreach($recent_views as $recent)
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="{{ url('product',$recent->id) }}"><img src="{{ thumb($recent->image) }}" alt="{{ $recent->name }}"></a>
														<a title="Quick View" href="{{ url('product',$recent->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
													</div>
													@if($recent->sell > 0)
														<div class="product-status">
															<span class="new">զեղչ</span>
														</div>
													@endif
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="{{ url('product',$recent->id) }}">{{  str_limit($recent->name, $limit = 30, $end = '...') }}</a>
													</div>
													<div class="price-box">
														@if($recent->sell > 0)
															<span class="product-price">{{ $recent->sell }} դր</span>
															<span class="product-price-old">{{ $recent->price }} դր</span>
														@else
															<span class="product-price">{{ $recent->price }} դր</span>
														@endif
													</div>
													<div class="product-star">
														<span>{{ $recent->category->name }}</span>
													</div>
												</div>
											</div>
										</li>
									@endforeach
								</ul>
							</div>
						</div>
					@endif
				</div>
				<div class="col-sm-8 col-md-9 col-lg-9">
					<div class="block block-new-arrivals block-vertical-menu">
						<div class="block-head">
							<h3 class="block-title">Նորույթներ</h3>
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1">
									<ul class="products" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"768":{"items":2},"1000":{"items":3},"1200":{"items":4}}'>
										@foreach($news as $new)
											<li class="col-md-3 col-sm-6 product news">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="{{ url('product',$new->id) }}"><img src="{{ thumb($new->image) }}" alt="{{ $new->name }}"></a>
															<a title="Դիտել" href="{{ url('product',$new->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
														</div>
														@if($new->sell > 0)
															<div class="product-status">
																<span class="new">զեղչ</span>
															</div>
														@endif
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="{{ url('product',$new->id) }}">{{ str_limit($new->name, $limit = 45, $end = '...') }}</a>
														</div>

														<div class="price-box">
															@if($new->sell > 0)
																<span class="product-price">{{ $new->sell }} դր</span>
																<span class="product-price-old">{{ $new->price }} դր</span>
															@else
																<span class="product-price">{{ $new->price }} դր</span>
															@endif
														</div>
														<div class="product-star">
															<span>{{ $new->category->name }}</span>
														</div>
													</div>
												</div>
											</li>
										@endforeach
									</ul>
								</div>

							</div>
						</div>
					</div>
					<div class="block3 block-hotdeals">
						<div class="block-head">
							<h3 class="block-title">Տեսականի</h3>
							<a class="link-all" href="{{ url('products') }}">ամբողջ տեսականին</a>
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1">
									<ul class="products" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"768":{"items":2},"1000":{"items":3},"1200":{"items":4}}'>
										@foreach($products as $product)
											<li class="col-md-3 col-sm-6 product news">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="{{ url('product',$product->id) }}"><img src="{{ thumb($product->image) }}" alt="{{ $product->name }}"></a>
															<a title="Դիտել" href="{{ url('product',$product->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
														</div>
														@if($product->sell > 0)
															<div class="product-status">
																<span class="new">զեղչ</span>
															</div>
														@endif
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="{{ url('product',$product->id) }}">{{ str_limit($product->name, $limit = 45, $end = '...') }}</a>
														</div>

														<div class="price-box">
															@if($product->sell > 0)
																<span class="product-price">{{ $product->sell }} դր</span>
																<span class="product-price-old">{{ $product->price }} դր</span>
															@else
																<span class="product-price">{{ $product->price }} դր</span>
															@endif
														</div>
														<div class="product-star">
															<span>{{ $product->category->name }}</span>
														</div>
													</div>
												</div>
											</li>
										@endforeach
									</ul>
								</div>

							</div>
						</div>
					</div>
					<div class="block3 block-hotdeals">
						<div class="block-head">
							<h3 class="block-title">Առավել դիտված</h3>
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1">
									<ul class="products" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"768":{"items":2},"1000":{"items":3},"1200":{"items":4}}'>
										@foreach($most_viewed as $viewed)
											<li class="col-md-3 col-sm-6 product news">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="{{ url('product',$viewed->id) }}"><img src="{{ thumb($viewed->image) }}" alt="{{ $viewed->name }}"></a>
															<a title="Դիտել" href="{{ url('product',$viewed->id) }}" class="btn-quick-view"><i class="fa fa-link"></i>Դիտել</a>
														</div>
														@if($viewed->sell > 0)
															<div class="product-status">
																<span class="new">զեղչ</span>
															</div>
														@endif
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="{{ url('product',$viewed->id) }}">{{ str_limit($viewed->name, $limit = 45, $end = '...') }}</a>
														</div>

														<div class="price-box">
															@if($viewed->sell > 0)
																<span class="product-price">{{ $viewed->sell }} դր</span>
																<span class="product-price-old">{{ $viewed->price }} դր</span>
															@else
																<span class="product-price">{{ $viewed->price }} դր</span>
															@endif
														</div>
														<div class="product-star">
															<span>{{ $viewed->category->name }}</span>
														</div>
													</div>
												</div>
											</li>
										@endforeach
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<!-- popular cat -->
			<div class="block-popular-cat2">
				<h3 class="title">Լավագույնները</h3>
				@foreach($bests as $key => $best)
					<div class="block block-popular-cat2-item">
						<div class="block-inner" style="border-color:{{ color() }}">
							<div class="cat-name">{{ $key }}</div>
							<div class="box-subcat">
								<ul class="list-subcat kt-owl-carousel" data-margin="20" data-loop="false" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":7}}'>
									@foreach($best as $item)
										<li class="item"><a href="{{ url('product',$item->id) }}"><img src="{{ thumb($item->image) }}" alt="Cat"></a></li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<!-- ./popular cat -->
		</div>
	</div>

@endsection
